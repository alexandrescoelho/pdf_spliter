﻿

using PDF_Spilter.model;
using System.IO;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace PDF_Spilter.service
{
    public static class SerializerServiceExtensions
    {
        public static void WriteXMLObject<ProcessConfiguration>(this ProcessConfiguration config, string fileName)
        {
            FileStream writer = new FileStream(fileName, FileMode.Create);
            XmlSerializer ser =
                new XmlSerializer(typeof(ProcessConfiguration));
            ser.Serialize(writer, config);
            writer.Close();
        }

        public static ProcessConfiguration ReadXMLObject(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open);
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ProcessConfiguration));
            XmlReader xmlReader = XmlReader.Create(fs);
            var config = xmlSerializer.Deserialize(xmlReader);
            fs.Close();

            return (ProcessConfiguration)config;
        }

        public static T ReadJsonObject<T>(string json)
        {
            var jss = new JavaScriptSerializer();
            T sData = jss.Deserialize<T>(json);

            return sData;
        }

        public static string WriteJsonObject<T>(T obj)
        {
            var jss = new JavaScriptSerializer();
            return jss.Serialize(obj);

        }
    }
}
