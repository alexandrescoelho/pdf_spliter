﻿using log4net;
using PDFXCoreAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDF_Spilter.service
{
    public class PDFService : IDisposable
    {
        private static log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string licKey = "LzI74tpPpCi/W4O0EDVcyPFbuHm2IabmC1btr1BehXgQjA5FoTBgJNovsIB3h5J6HY8Y3M2t+eRFjobTUQmxQajNGGcrxMzsP+k7CaIAY+w/Wwq3IzdqoM/wluNnIn+q4RDH5g2hmBsAB8hc7k8KHDoXJOjars6fy3z8y8/BqxDOpg2QpH/iJEUgAGW47dU/3COsjX+00SbxNnI+a2HRJSQ4SuNmbJRzDDu9gRtXrs29FosGQ916Fbjpcvkbu7LYKYy+lST7i1IpueTPqYa4CErl6jLA3cavMxTY7/fe4JsATqnR4nTphEvJdc0k/dbles86AwBL4TUSs2ldSKAKVwCHK/KM3MYm14dao2j16OgvIgm4U/mOyKin7UMFFgFeOJfcpDjm0OuX4dPwoT7VkGIVK29ZOZtHDRBep8vV3XMcvsz0HvPsOKTln23auGA1s5S5ONglqTJSgNlD4Pt9ildjga/4g9xsV37YeAbxqmeLba1eXfMg+4L244CfOBM9GUgvmnEcpDaCVkDCUc09yOUwxdwH5wdbrfVdzlHv2+Pr/YYpnRhTMnqo1JNcIw==";

        private IPXC_Inst pxcInst;
        private IPXC_Document doc = null;
        private bool disposed = false;

        public string PdfFile { get; private set; }

        public PDFService(string file)
        {
            PdfFile = file;
            InitializePDFApi();
        }

        private void InitializePDFApi()
        {
            try
            {
                log.Debug("Initializing API");

                pxcInst = new PXC_Inst();
                pxcInst.Init(licKey);
                doc = pxcInst.OpenDocumentFromFile(PdfFile, null);
            }
            catch (Exception e)
            {
                log.Error("Error: initializing API", e);
                throw;
            }


        }

        public uint GetPageCount()
        {
            return doc.Pages.Count;
        }

        public void SavePageAsFile(string file, uint startPageIndex, uint countPages = 1)
        {
            try
            {
                log.Info(string.Format("Extracting page {0} until {1} as pdf...", startPageIndex+1,startPageIndex+1+countPages-1));
                var d = pxcInst.NewDocument();
                d.Pages.InsertPagesFromDoc(doc, 0, startPageIndex, countPages, (uint)PXC_InsertPagesFlags.IPF_Annots_Copy | (uint)PXC_InsertPagesFlags.IPF_Bookmarks_CopyAll);
                d.WriteToFile(file);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Fail to spilt page {0} (pdf) to save as {1}", startPageIndex, file), ex);
                throw;
            }
        }

        public void SavePageAsText(string file, uint startPageIndex, uint countPages = 1)
        {
            try
            {
                log.Info(string.Format("Extracting page {0} until {1} as text...", startPageIndex + 1, startPageIndex + 1 + countPages - 1));
                using (var sw = new StreamWriter(file, false, Encoding.UTF8))
                {
                    StringBuilder sb = new StringBuilder();
                    for (uint i = startPageIndex; i < startPageIndex+countPages; i++)
                    {
                        var page = doc.Pages[i];
                        sb.Append( GetText(page));
                    }

                    sw.Write(sb.ToString());
                    sw.Flush();
                }
                    
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Fail to spilt page {0} (text) to save as {1}", startPageIndex, file), ex);
                throw;
            }
        }

        public string GetText()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                log.Debug("Extracting content");

                IPXC_Pages pages = doc.Pages;

                for (uint i = 0; i < pages.Count; i++)
                {
                    IPXC_Page page = pages[(uint)i];
                    IPXC_GetPageTextOptions options = pxcInst.CreateGetPageTextOptions((uint)1);
                    IPXC_PageText texts = page.GetText(options);

                    for (uint j = 0; j < texts.CharsCount; j++)
                    {
                        if ((texts.get_CharFlags(j) == (uint)PXC_TextCharFlags.TCF_LineBegin) & (j > 1))
                            sb.Append(System.Environment.NewLine);

                        sb.Append(texts.GetChars(j, 1));
                        var aa = texts.CharQuad[j];
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Error: extracting content", e);
                throw;
            }


            return sb.ToString();
        }

        //public void SearchAndHighlightWords(List<SearchDTO> searchTerms)
        //{
        //    try
        //    {
        //        log.Debug("Searching strings to highlight");
        //        IPXS_Inst pxsInst = (IPXS_Inst)pxcInst.GetExtension("PXS");
        //        IAUX_Inst auxInst = pxcInst.GetExtension("AUX");

        //        for (uint i = 0; i < doc.Pages.Count; i++)
        //        {
        //            IPXC_Page page = doc.Pages[i];
        //            IPXC_QuadsF quads;
        //            var pageText = GetText(page);

        //            foreach (var searchItem in searchTerms)
        //            {
        //                var indexes = pageText.AllIndexesOf(searchItem.Text);

        //                if (indexes.Count == 0)
        //                    continue;

        //                PXC_Rect rcAnnot = page.get_Box(PXC_BoxType.PBox_BBox);
        //                uint nTextBox = pxsInst.StrToAtom("Highlight");
        //                IPXC_Annotation Annot = page.InsertNewAnnot(nTextBox, ref rcAnnot, 0);

        //                if (Annot == null)
        //                    continue;

        //                quads = GetQuadsFromIndexes(page, indexes, searchItem.Text);
        //                IPXC_AnnotData_TextMarkup markup = (IPXC_AnnotData_TextMarkup)Annot.Data;
        //                var fColor = auxInst.CreateColor(ColorType.ColorType_RGB);
        //                var color = searchItem.GetRGBColorAsPercents();
        //                fColor.SetRGB(color.Item1, color.Item2, color.Item3);
        //                markup.Color = fColor;
        //                markup.Quads = quads;
        //                Annot.Data = markup;
        //                IPXC_AnnotsList al = pxcInst.CreateAnnotsList();
        //                al.Insert(Annot);
        //                doc.FlattenAnnotations(0, al);
        //            }

        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("Error: searching strings to highlight", e);
        //        throw;
        //    }
        //}

        /// <summary>
        /// Save changes to the current file  
        /// </summary>
        /// <param name="file">Pass the file path in case you want to Save As</param>
        public void SaveFile(string file = null)
        {
            if (string.IsNullOrEmpty(file))
                doc.WriteTo(null);
            else
                doc.WriteToFile(file);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                doc.Close();
                pxcInst.Finalize();
                //pxvInst.Shutdown();
                //pxvInst = null;
                //doc = null;
                //pxcInst = null;
            }

            // Free any unmanaged objects here.
            disposed = true;
        }

        private string GetText(IPXC_Page page)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                IPXC_PageText texts = page.GetText(null);

                for (uint j = 0; j < texts.CharsCount; j++)
                {
                    sb.Append(texts.GetChars(j, 1));
                }

            }
            catch (Exception e)
            {
                throw;
            }

            return sb.ToString();
        }

        private IPXC_QuadsF GetQuadsFromIndexes(IPXC_Page page, List<int> indexes, string v)
        {
            IAUX_Inst auxInst = pxcInst.GetExtension("AUX");
            var pageText = page.GetText(null);
            var quads = pxcInst.CreateQuads();
            PXC_RectF rec1;
            PXC_QuadF q;

            foreach (var index in indexes)
            {
                try
                {
                    q = new PXC_QuadF();
                    var rec = new PXC_RectF();

                    for (int i = 0; i < v.Length; i++)
                    {
                        q = pageText.CharQuad[Convert.ToUInt16(index + i)];
                        rec1 = auxInst.MathHelper.Quad_To_RectF(q);
                        if (rec.left == 0f && rec.bottom == 0f && rec.right == 0f && rec.top == 0f)
                            rec = rec1;
                        else
                            rec = auxInst.MathHelper.Rect_UnionF(rec, rec1);
                    }

                    q = auxInst.MathHelper.Rect_To_QuadF(rec);
                    quads.Insert(q);
                }
                catch (Exception)
                {
                    break;
                }
            }

            return quads;
        }

    }
}
