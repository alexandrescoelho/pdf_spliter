﻿using log4net;
using PDF_Spilter.application;
using PDF_Spilter.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDF_Spilter
{
    class Program
    {
        private static log4net.ILog log;

        static void Main(string[] args)
        {
            try
            {
                //Load config file
                if (args.Length != 1)
                    throw new ArgumentNullException("You must specify the configuration file path");
                var xml = SerializerServiceExtensions.ReadXMLObject(args[0]);

                GlobalContext.Properties["LogFilePath"] = xml.Definition.LogPath;
                log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Info("Configuration file successfully loaded");
            }
            catch (Exception)
            {

                throw;
            }

            log.Info("Application started....");
            //XmlConfigurator.Configure();
            var app = new PDFApplication(args[0]);
            app.Start();
            GC.SuppressFinalize(app);
            log.Info("Application is shutting down...");

        }
    }
}
