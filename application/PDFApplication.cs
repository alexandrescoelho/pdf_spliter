﻿using log4net;
using PDF_Spilter.model;
using PDF_Spilter.service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDF_Spilter.application
{
    public class PDFApplication
    {
        private static log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string InputXmlFile { get; set; }

        public PDFApplication(string inputXmlFile)
        {
            InputXmlFile = inputXmlFile;
        }

        public void Start()
        {
            log.Debug("loading configuration xml file at " + InputXmlFile);
            ProcessConfiguration config = SerializerServiceExtensions.ReadXMLObject(InputXmlFile);

            foreach (var file in config.Files)
            {
                try
                {
                    log.Info(string.Format("processing file [{0}] of [{1}]", config.Files.ToList().IndexOf(file) + 1, config.Files.ToList().Count));

                    if (!File.Exists(file.Path))
                        log.Error("File not found " + file.Path);
                    ExtractDataFromFile(file.Path, config.Definition.Suffix);

                }
                catch (Exception ex)
                {

                    log.Error("The split proccess has failed for " + file.Path, ex);
                }
            }
        }

            public void ExtractPagesAndTextFromPDF(string path, string pagePreffix = "_PAG")
        {

            FileAttributes attr = File.GetAttributes(path);

            if (attr.HasFlag(FileAttributes.Directory))
                ExtractDataFromDirectory(path, pagePreffix);
            else
                ExtractDataFromFile(path, pagePreffix);

        }

        private void ExtractDataFromFile(string path, string pagePreffix="_PAG")
        {
            if (!File.Exists(path))
                return;

            PDFService service = new PDFService(path);

            var totalPages = service.GetPageCount();
            var fileName = Path.GetFileNameWithoutExtension(path);
            var directory = Path.GetDirectoryName(path);
            var fileNameTemplate = Path.Combine(directory, fileName + "{0}{1}");
            
            for (int i = 0; i < totalPages; i++)
            {
                var finalFileName = string.Format(fileNameTemplate, pagePreffix, i+1);

                service.SavePageAsFile(finalFileName + ".pdf", (uint)i);
                service.SavePageAsText(finalFileName + ".txt", (uint)i);
            }
            
        }

        private void ExtractDataFromDirectory(string path, string pagePreffix)
        {
            var files = Directory.GetFiles(path, "*.pdf", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                log.Info("Processing file " + file);
                ExtractDataFromFile(file);
            }
        }
    }
}
