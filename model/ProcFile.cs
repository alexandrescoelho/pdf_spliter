﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PDF_Spilter.model
{
    [XmlRoot("file")]
    public class ProcFile
    {
        [XmlAttribute("pdf_file")]
        public string Path { get; set; }

    }
}
