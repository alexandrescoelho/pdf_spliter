﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace PDF_Spilter.model
{
    [XmlRoot("config", Namespace = "http://schemas.datacontract.org/2004/07/PDFSpliter.Model")]
    public class ProcessConfiguration : IExtensibleDataObject
    {
        [XmlElement("definition")]
        public Params Definition { get; set; }

        [XmlArray("files")]
        [XmlArrayItem("file")]
        public List<ProcFile> Files { get; set; }

        private ExtensionDataObject extensionData_Value;
        public ExtensionDataObject ExtensionData
        {
            get
            {
                return extensionData_Value;
            }
            set
            {
                extensionData_Value = value;
            }
        }

    }
}
