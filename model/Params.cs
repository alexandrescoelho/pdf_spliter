﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PDF_Spilter.model
{
    [XmlRoot(ElementName ="definition")]
    public class Params
    {
        [XmlAttribute("log_path")]
        public string LogPath { get; set; }
        [XmlAttribute("page_suffix")]
        public string Suffix { get; set; }

    }
}
